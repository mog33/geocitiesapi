# Geo Cities API

Simple city search service based on Geonames data.
  
## Informations

### Version

1.0

### License

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

### Contact

## Content negotiation

### URI Schemes

* http

### Consumes

* text/plain

### Produces

* application/json
* text/plain

## All endpoints

### cities

| Method  | URI     | Name   | Summary |
|---------|---------|--------|---------|
| GET | /api/v1/cities | [get API v1 cities](#get-api-v1-cities) | Search cities |
| GET | /api/v1/city/{id} | [get API v1 city ID](#get-api-v1-city-id) | Get City |
  
### status

| Method  | URI     | Name   | Summary |
|---------|---------|--------|---------|
| GET | /healthz | [get healthz](#get-healthz) | Service health |
| GET | /started | [get started](#get-started) | Service started |
  
## Paths

### <span id="get-api-v1-cities"></span> Search cities (*GetAPIV1Cities*)

```
GET /api/v1/cities
```

Search cities by name and country ISO code

#### Consumes

* text/plain; charset=utf-8

#### Produces

* application/json

#### Parameters

| Name | Source | Type | Go type | Separator | Required | Default | Description |
|------|--------|------|---------|-----------| :------: |---------|-------------|
| country | `query` | string | `string` |  | ✓ |  | Country ISO code |
| q | `query` | string | `string` |  | ✓ |  | Search by city name |

#### All responses

| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#get-api-v1-cities-200) | OK | OK |  | [schema](#get-api-v1-cities-200-schema) |
| [400](#get-api-v1-cities-400) | Bad Request | Bad Request |  | [schema](#get-api-v1-cities-400-schema) |
| [404](#get-api-v1-cities-404) | Not Found | Not Found |  | [schema](#get-api-v1-cities-404-schema) |

#### Responses

##### <span id="get-api-v1-cities-200"></span> 200 - OK

Status: OK

###### <span id="get-api-v1-cities-200-schema"></span> Schema

[ModelsCityList](#models-city-list)

##### <span id="get-api-v1-cities-400"></span> 400 - Bad Request

Status: Bad Request

###### <span id="get-api-v1-cities-400-schema"></span> Schema

any

##### <span id="get-api-v1-cities-404"></span> 404 - Not Found

Status: Not Found

###### <span id="get-api-v1-cities-404-schema"></span> Schema

any

### <span id="get-api-v1-city-id"></span> Get City (*GetAPIV1CityID*)

```
GET /api/v1/city/{id}
```

Get City by name and country ISO code

#### Consumes

* text/plain; charset=utf-8

#### Produces

* application/json

#### Parameters

| Name | Source | Type | Go type | Separator | Required | Default | Description |
|------|--------|------|---------|-----------| :------: |---------|-------------|
| id | `path` | string | `string` |  | ✓ |  | City geoname id |

#### All responses

| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#get-api-v1-city-id-200) | OK | OK |  | [schema](#get-api-v1-city-id-200-schema) |
| [400](#get-api-v1-city-id-400) | Bad Request | Bad Request |  | [schema](#get-api-v1-city-id-400-schema) |
| [404](#get-api-v1-city-id-404) | Not Found | Not Found |  | [schema](#get-api-v1-city-id-404-schema) |

#### Responses

##### <span id="get-api-v1-city-id-200"></span> 200 - OK

Status: OK

###### <span id="get-api-v1-city-id-200-schema"></span> Schema

[ModelsCityList](#models-city-list)

##### <span id="get-api-v1-city-id-400"></span> 400 - Bad Request

Status: Bad Request

###### <span id="get-api-v1-city-id-400-schema"></span> Schema

any

##### <span id="get-api-v1-city-id-404"></span> 404 - Not Found

Status: Not Found

###### <span id="get-api-v1-city-id-404-schema"></span> Schema

any

### <span id="get-healthz"></span> Service health (*GetHealthz*)

```
GET /healthz
```

Check service and backend health

#### Produces

* text/plain; charset=utf-8

#### All responses

| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#get-healthz-200) | OK | OK |  | [schema](#get-healthz-200-schema) |
| [404](#get-healthz-404) | Not Found | Not Found |  | [schema](#get-healthz-404-schema) |

#### Responses

##### <span id="get-healthz-200"></span> 200 - OK

Status: OK

###### <span id="get-healthz-200-schema"></span> Schema

[ModelsCityList](#models-city-list)

##### <span id="get-healthz-404"></span> 404 - Not Found

Status: Not Found

###### <span id="get-healthz-404-schema"></span> Schema

any

### <span id="get-started"></span> Service started (*GetStarted*)

```
GET /started
```

Check if service is started and running

#### Produces

* text/plain; charset=utf-8

#### All responses

| Code | Status | Description | Has headers | Schema |
|------|--------|-------------|:-----------:|--------|
| [200](#get-started-200) | OK | OK |  | [schema](#get-started-200-schema) |
| [404](#get-started-404) | Not Found | Not Found |  | [schema](#get-started-404-schema) |

#### Responses

##### <span id="get-started-200"></span> 200 - OK

Status: OK

###### <span id="get-started-200-schema"></span> Schema

[ModelsCityList](#models-city-list)

##### <span id="get-started-404"></span> 404 - Not Found

Status: Not Found

###### <span id="get-started-404-schema"></span> Schema

any

## Models

### <span id="models-city"></span> models.City

> City information.
  
**Properties**

| Name | Type | Go type | Required | Default | Description | Example |
|------|------|---------|:--------:| ------- |-------------|---------|
| asciiname | string| `string` |  | |  | `Bolquere` |
| country | string| `string` |  | |  | `France` |
| country_code | string| `string` |  | |  | `FR` |
| geonameid | string| `string` |  | |  | `3031859` |
| name | string| `string` |  | |  | `Bolquère` |
| state | string| `string` |  | |  | `3031859` |

### <span id="models-city-list"></span> models.CityList

> List of Cities.
  
**Properties**

| Name | Type | Go type | Required | Default | Description | Example |
|------|------|---------|:--------:| ------- |-------------|---------|
| cities | [][ModelsCity](#models-city)| `[]*ModelsCity` |  | |  |  |
