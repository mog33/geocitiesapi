package models

// @Description List of Cities.
type CityList struct {
	Cities []City `json:"cities"`
}
