package models

// @Description City information.
type City struct {
	GeonameId   string `json:"geonameid" example:"3031859"`
	Name        string `json:"name" example:"Bolquère"`
	Asciiname   string `json:"asciiname" example:"Bolquere"`
	State       string `json:"state" example:"3031859"`
	Country     string `json:"country" example:"France"`
	CountryCode string `json:"country_code" example:"FR"`
}
