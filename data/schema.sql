-- geonameid;name;asciiname;state;country_code;country
DROP TABLE IF EXISTS cities;
CREATE TABLE "cities" (
  "geonameid" integer NOT NULL,
  "name" text NOT NULL,
  "asciiname" text NULL,
  "state" text NULL,
  "country_code" text NOT NULL,
  "country" text NULL
);
