import csv
import json
import sys
import argparse
import os
from urllib import request
import zipfile

''' Geonames parameters. '''
geo_source = "https://download.geonames.org/export/dump/"
geo_admincodes = 'admin1CodesASCII.txt'
geo_countries = 'countryInfo.txt'

''' Prefix of the file for cities on Geonames and headers '''
cities = "cities"
cities_header = ['geonameid', 'name', 'asciiname', 'state', 'country_code', 'country']
cities_cols = [0, 1, 2, 'state_from_code', 8, 'country_from_code']

''' Create files in folder data at the root '''
script = os.path.dirname(os.path.realpath(__file__)) + "/"

force=False

''' Download and extract files from Geonames '''
def dl():
  _download(geo_admincodes)
  _download(geo_countries)

  filename = cities + str(population)
  _download(filename + '.zip')

  if os.path.exists(filename + '.txt') and not force:
    print(f'[info] Skip file {filename} already exist, use -f to force extraction from zip.')
    return

  print(f'[notice] Extract {filename}')
  with zipfile.ZipFile(filename + '.zip', 'r') as zip_ref:
    zip_ref.extractall()
    os.rename(filename + '.txt', cities + '.txt')

def _download(filename):
  if os.path.exists(filename) and not force:
    print(f'[info] Skip file {filename} already exist, use -f to force download.')
    return

  url = geo_source + filename
  print(f'[notice] Downloading {url}', end='... ')
  r = request.urlretrieve(url, filename)
  print('Done!')

''' Remap and create CSV separated by semi colon '''
def csv2csv():
  _fixCsv(cities + '.txt', cities + '.csv', cities_header, cities_cols)

def _fixCsv(source, target, header, cols):
  print(f'[notice] Process {source} > {target}')

  _get_states_list()
  _get_countries_list()

  source = open(source)
  csv_source = csv.reader(source, delimiter='\t')

  with open(target, 'w', encoding='UTF8') as csvFormattedFile:
    writer = csv.writer(csvFormattedFile, delimiter=';')
    ''' Add our header '''
    writer.writerow(header)
    for row in csv_source:
      rowTowrite = []
      for col in cols:
        if col == 'state_from_code':
          ''' State code is optional '''
          if not row[10]:
            if verbose:
              print(f'[notice] Missing numeric state code for: {row[1]} ({row[0]}), country: {row[8]}')
            rowTowrite.append('')
            continue
          codeState = row[8] + "." + row[10]
          if codeState in states_names:
            rowTowrite.append(states_names[codeState])
          else:
            if verbose:
              print(f'[notice] Unknown state code: {codeState} for: {row[1]} ({row[0]})')
            rowTowrite.append('')
        elif col == 'country_from_code':
          codeCountry = row[8]
          if codeCountry in countries_names:
            rowTowrite.append(countries_names[codeCountry])
          else:
            if verbose:
              print(f'[notice] Unknown country code: {codeCountry}')
            rowTowrite.append('')
        else:
          rowTowrite.append(row[col])
      writer.writerow(rowTowrite)
  print('[success] Processing done!')

''' Create a list of states from admincodes, write a temp file to speed up loop '''
def _get_states_list():
  global states_names
  if os.path.exists('states.json') and not force:
    with open('states.json') as jsonStatefile:
      states_names = json.load(jsonStatefile)
  else:
    states_names = {}
    states_file = open(geo_admincodes)
    states_file = csv.reader(states_file, delimiter='\t')
    for row in states_file:
      states_names[row[0]] = row[1]

    with open('states.json', 'w') as outfile:
      json.dump(states_names, outfile)

''' Create a list of countries with code, write a temp file to speed up loop '''
def _get_countries_list():
  global countries_names
  if os.path.exists('countries.json') and not force:
    with open('countries.json') as jsonStatefile:
      countries_names = json.load(jsonStatefile)
  else:
    countries_names = {}
    countries_file = open(geo_countries)
    countries_file = csv.reader(_clean_comments(countries_file), delimiter='\t')
    for row in countries_file:
      countries_names[row[0]] = row[4]

    with open('countries.json', 'w') as outfile:
      json.dump(countries_names, outfile)

def _clean_comments(csvfile):
  for row in csvfile:
    raw = row.split('#')[0].strip()
    if raw: yield raw

def csv2json():
  src = cities + '.csv'
  dest = cities + '.json'
  _checkFile(src)
  _csv2json(src, dest)

def _checkFile(filename):
  if not os.path.exists(filename):
    print(f'[ERROR] File {filename} do not exist!')
    sys.exit(os.EX_OSFILE)

def _csv2json(source, target):
  print(f'[notice] Create {target} from {source}')
  data = []
  with open(source, encoding='utf-8') as csvFile:
    csvReader = csv.DictReader(csvFile, delimiter=';')
    for row in csvReader:
      data.append(row)
  with open(target, 'w', encoding='utf-8') as jsonFile:
    jsonFile.write(json.dumps(data))
  print('[success] Done!')

''' Create one json files by country '''
def splitJson():
  print('[notice] Creating splitted Json files')

  destination = script + data_dest + 'json/'
  os.makedirs(destination, exist_ok=True)

  with open(cities + '.json') as jsonFullFile:
    jsonFullFile = json.load(jsonFullFile)

    try:
        countries_names
    except NameError:
        _get_countries_list()

    ''' Loop country code to create a file for each '''
    for countryCode in countries_names:
      jsonCountry = []
      full = open(cities + '.json')
      fullEntries = json.load(full)
      for entry in fullEntries:
        if entry['country_code'] == countryCode:
          jsonCountry.append(entry)

      ''' Write json file for this country '''
      with open(destination + cities + '_' + countryCode + '.json', 'w', encoding='utf-8') as jsonFile:
        jsonFile.write(json.dumps(jsonCountry))
      if verbose:
        print(f'[info] Isolated json file for country {countryCode}')

def copy():
  print('[notice] Copy files')
  try:
    os.rename(cities + '.csv', script + data_dest + cities + '.csv')
  except:
    print("[ERROR] Fail to create final csv file.")

def clean():
  print('[notice] Delete files')
  files = [
    geo_admincodes,
    geo_countries,
    cities + '.txt',
    cities + str(population) + '.zip',
    cities + '.json',
    'states.json',
    'countries.json',
  ]
  for file in files:
    if os.path.isfile(file):
      os.remove(file)
  print('[success] Done!')

def main():
  global population, data_dest, delete, force, verbose, reports

  action = 'run'
  parser = argparse.ArgumentParser(description='Download and process Geonames files for Geo Cities API.')

  parser.add_argument('action', nargs='?', default='run', help='''Optional single action, can be:
    dl (Download files), csv (Format CSV), c2j (Convert CSV to Json), sj (Split Json files), del (Delete source files).''')
  parser.add_argument('-p',
                      '--pop',
                      type=int,
                      default=1000,
                      help='Population of the cities, options are: 500, 1000, 5000, 15000. (Default: 1000)')
  parser.add_argument('-d',
                      type=str,
                      default="",
                      help='Data destination, relative to this script. Default is in this script dir.')
  parser.add_argument('-n',
                      action='store_false',
                      help='Do not delete source files after process.')
  parser.add_argument('-f',
                      action='store_true',
                      help='Force download even if file already exist.')
  parser.add_argument('-v',
                      action='store_true',
                      help='Verbose output.')

  args = parser.parse_args()
  action = args.action
  population = args.pop
  data_dest = args.d
  delete = args.n
  force = args.f
  verbose = args.v
  reports = {}

  if population not in (500, 1000, 5000, 15000):
    print('[ERROR] Invalid population size, choices are: 500, 1000, 5000, 15000')
    sys.exit()

  if action not in ('dl', 'csv', 'c2j', 'sj', 'del', 'run'):
    print('[ERROR] Invalid action, options are: dl, csv, c2j, sj, del')
    sys.exit()
  
  if action == 'dl':
    dl()
  elif action == 'csv':
    csv2csv()
  elif action == 'c2j':
    csv2json()
  elif action == 'sj':
    splitJson()
  elif action == 'del':
    clean()
  elif action == 'run':
    dl()
    csv2csv()
    copy()
    if delete:
      clean()

if __name__ == "__main__":
  main()
