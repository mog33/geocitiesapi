package auth

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

const (
	apiKeyHeader = "X-API-Key"
	apiKeyQuery  = "key"
)

// API key auth middleware.
func AuthAPIKey() gin.HandlerFunc {
	return func(c *gin.Context) {
		key := c.Request.Header.Get(apiKeyHeader)
		if key == "" {
			key = c.Request.URL.Query().Get(apiKeyQuery)
		}

		secret, err := getSecret()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"code":    http.StatusUnauthorized,
				"message": http.StatusText(http.StatusUnauthorized),
			})
			return
		}

		if secret != key {
			log.Println("Invalid API key!")
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"code":    http.StatusUnauthorized,
				"message": http.StatusText(http.StatusUnauthorized),
			})
			return
		}

		c.Next()
	}
}
