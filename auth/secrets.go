package auth

import (
	"log"
	"os"

	"github.com/pkg/errors"
)

func getSecret() (string, error) {
	apiKey := os.Getenv("API_KEY")
	if apiKey == "" {
		msg := "Missing env API_KEY value. Did you set it?"
		log.Println(msg)
		return "", errors.New(msg)
	}
	return apiKey, nil
}
