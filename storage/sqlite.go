package storage

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"
	"gitlab.com/mog33/geocitiesapi/models"
)

type SQLiteBackend struct {
	client *sql.DB
}

func (s *SQLiteBackend) Health() error {
	_, err := openSQLiteDB()
	if err != nil {
		return err
	}
	return nil
}

func (s *SQLiteBackend) SearchCities(search string, country string, limit int) (models.CityList, string, error) {
	var result models.CityList

	sqlStatement := `SELECT DISTINCT(geonameid), name, asciiname, state, country_code, country
		FROM cities
		WHERE name LIKE $1 || '%'
			AND country_code = $2
		LIMIT $3`

	rows, err := s.client.Query(sqlStatement, search, country, limit)
	if err != nil {
		return result, "Query error.", err
	}

	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&geonameid, &name, &asciiname, &state, &country_code, &country)
		if err != nil {
			return result, "Rows scan error.", err
		}
		result.Cities = append(result.Cities, models.City{
			GeonameId:   geonameid,
			Name:        name,
			Asciiname:   asciiname,
			State:       state,
			CountryCode: country_code,
			Country:     country,
		})
	}

	if len(result.Cities) == 0 {
		return result, "No results found.", errors.New(fmt.Sprintf("No result found for search '%s', %s", search, country))
	}
	return result, "Success", nil
}

func (s *SQLiteBackend) GetCity(geonameid string) (models.City, error) {
	var result models.City

	sqlStatement := `SELECT geonameid, name, asciiname, state, country_code, country
		FROM cities
		WHERE cities.geonameid = $1`

	err := s.client.QueryRow(sqlStatement, geonameid).Scan(
		&result.GeonameId,
		&result.Name,
		&result.Asciiname,
		&result.State,
		&result.CountryCode,
		&result.Country,
	)
	if err == sql.ErrNoRows {
		return result, errors.New(fmt.Sprintf("No result found for id %s", geonameid))
	} else if err != nil {
		return result, err
	} else {
		return result, nil
	}
}

func NewSQLiteBackend() (Backend, error) {
	client, _ := openSQLiteDB()
	return &SQLiteBackend{
		client: client,
	}, nil
}

func openSQLiteDB() (*sql.DB, error) {
	dbFile := "file:" + os.Getenv("API_SQLITE_DB")
	db, err := sql.Open("sqlite3", dbFile)
	if err != nil {
		return nil, err
	}
	return db, nil
}
