package storage

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/mog33/geocitiesapi/models"
)

type PgsqlBackend struct {
	client *sql.DB
}

func (p *PgsqlBackend) Health() error {
	err := p.client.Ping()
	if err != nil {
		return err
	}
	return nil
}

func (p *PgsqlBackend) SearchCities(search string, country string, limit int) (models.CityList, string, error) {
	var result models.CityList

	sqlStatement := `SELECT DISTINCT(geonameid), name, asciiname, state, country_code, country
		FROM cities
		WHERE name ILIKE $1 || '%'
			AND country_code = $2
		LIMIT $3`

	rows, err := p.client.Query(sqlStatement, search, country, limit)
	if err != nil {
		return result, "Query error.", err
	}

	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&geonameid, &name, &asciiname, &state, &country_code, &country)
		if err != nil {
			return result, "Rows scan error.", err
		}
		result.Cities = append(result.Cities, models.City{
			GeonameId:   geonameid,
			Name:        name,
			Asciiname:   asciiname,
			State:       state,
			CountryCode: country_code,
			Country:     country,
		})
	}

	if len(result.Cities) == 0 {
		return result, "No results found.", nil
	}
	return result, "Success", nil
}

func (p *PgsqlBackend) GetCity(geonameid string) (models.City, error) {
	var result models.City

	sqlStatement := `SELECT geonameid, name, asciiname, state, country_code, country
		FROM cities
		WHERE cities.geonameid = $1`

	err := p.client.QueryRow(sqlStatement, geonameid).Scan(
		&result.GeonameId,
		&result.Name,
		&result.Asciiname,
		&result.State,
		&result.CountryCode,
		&result.Country,
	)
	if err == sql.ErrNoRows {
		return result, errors.New(fmt.Sprintf("No result found for id %s", geonameid))
	} else if err != nil {
		return result, err
	} else {
		return result, nil
	}
}

func NewPgsqlBackend() (Backend, error) {
	client, _ := connectPgsqlDB()
	return &PgsqlBackend{
		client: client,
	}, nil
}

func connectPgsqlDB() (*sql.DB, error) {
	conf := fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
		os.Getenv("API_POSTGRES_HOST"),
		os.Getenv("API_POSTGRES_PORT"),
		os.Getenv("API_POSTGRES_DATABASE"),
		os.Getenv("API_POSTGRES_USER"),
		os.Getenv("API_POSTGRES_PASSWORD"),
	)
	db, err := sql.Open("postgres", conf)
	if err != nil {
		return nil, err
	}
	return db, nil
}
