package storage

import (
	"log"
	"os"

	"gitlab.com/mog33/geocitiesapi/models"
)

var (
	backend      Backend
	geonameid    string
	name         string
	asciiname    string
	state        string
	country_code string
)

type Backend interface {
	Health() error
	GetCity(geonameid string) (models.City, error)
	SearchCities(search string, country string, limit int) (models.CityList, string, error)
}

func GetBackend() Backend {
	storageType := os.Getenv("API_BACKEND")
	if storageType == "" {
		storageType = "sqlite"
	}
	switch storageType {
	case "pgsql":
		backend, _ = NewPgsqlBackend()
	case "sqlite":
		backend, _ = NewSQLiteBackend()
	}
	return backend
}

func CheckBackend() {
	storageType := os.Getenv("API_BACKEND")
	err := GetBackend().Health()
	if err != nil {
		log.Printf("[error] Backend %s status is bad: %s", storageType, err.Error())
		return
	}
	log.Printf("[success] Backend %s status ok!", storageType)
}
