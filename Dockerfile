FROM golang:1.18.1 as builder

WORKDIR /geocitiesapi/

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -tags=nomsgpack -o bin/geocitiesapi main.go

FROM alpine:latest

WORKDIR /geocitiesapi

COPY --from=builder /geocitiesapi/ /geocitiesapi/

EXPOSE 8181

CMD ./bin/geocitiesapi