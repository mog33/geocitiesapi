# Go Geo cities API

Simple microservice to provide a City search based on Geonames data.

See Swagger API: [./docs/README.md](./docs/README.md)

## Script to download and format Geonames data

_Note_ Parameter '-p' is the optional minimum population of the city to get,
can be 500, 1000, 5000 or 15000 (default: 1000).

```bash
cd data
python get-data.py
```

## Local setup with SQLite

Create and import db:

```bash
cd data
sqlite3 cities.db
.mode csv
.separator ";"
.import cities.csv cities
.quit
```

## Local PostgreSQL database

```bash
docker run --name geodb -e POSTGRES_PASSWORD=cities -e POSTGRES_USER=cities -p 5432:5432 -v $(pwd)/data:/data -d postgres:13
```

### Import csv data in PostgreSQL locally

Create db schema

```bash
docker exec geodb psql -U cities -d cities -f '/data/schema.sql'
```

Import data in db from csv file

```bash
docker exec geodb psql -U cities -d cities -c 'TRUNCATE "cities";'
docker exec geodb psql -U cities -d cities -f '/data/copy.sql'
```

### Post step

Export the db in `./data` to be able to import the values faster.

## Dev

Copy `.env.sample` as `.env`, and edit.

For **PostgreSQL** usage, launch the stack:

```bash
docker compose up
```

Launch app locally

```bash
go run main.go
```

## Build

Build target is linux/amd64.

```bash
make build
```

Include file in `Dockerfile.bin`

```bash
docker build -f Dockerfile.bin -t geocitiesapi .
docker run -d -p 8181:8181 geocitiesapi
```

## Information on data from Geonames

<https://download.geonames.org/export/dump/>

Admin code 1 (State, region)

- <https://download.geonames.org/export/dump/admin1CodesASCII.txt>

Column csv:

```text
c1 = code
c2 = name
c3 = asciiname
```

Cities source:

- <https://download.geonames.org/export/dump/cities500.zip>
- <https://download.geonames.org/export/dump/cities1000.zip>
- <https://download.geonames.org/export/dump/cities5000.zip>
- <https://download.geonames.org/export/dump/cities15000.zip>

Column csv:

```text
c1 = geonameid
c2 = name
c3 = asciiname
c9 = country_code
c11 = admin1_code
```
