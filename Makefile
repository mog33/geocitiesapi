all: deps build docs

.PHONY: deps
deps:
	go get

.PHONY: build
build: deps
	GOOS=linux GOARCH=amd64 go build -tags=nomsgpack -o bin/geocitiesapi main.go

.PHONY: docs
docs:
	swag init
	swagger generate markdown -f ./docs/swagger.yaml && mv -f markdown.md ./docs/README.md
