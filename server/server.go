package server

import (
	"log"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
)

func StartServer(apiVersion string) {

	prod := os.Getenv("API_PROD")
	if prod == "1" {
		gin.SetMode(gin.ReleaseMode)
	}

	color := os.Getenv("API_ENABLE_COLOR")
	if color == "0" {
		gin.DisableConsoleColor()
	}

	// Include Logger and Recovery.
	router := gin.Default()

	trustedProxies := os.Getenv("API_TRUSTED_PROXIES")
	if trustedProxies != "" {
		log.Printf("[notice] Trusted proxies: %s", trustedProxies)
		router.SetTrustedProxies(strings.Split(trustedProxies, ","))
	}

	Routing(router, apiVersion)

	httpPort := os.Getenv("API_HTTP_PORT")
	if httpPort == "" {
		httpPort = "8181"
	}
	log.Printf("[notice] Sever started port %s", httpPort)
	router.Run(":" + httpPort)
}
