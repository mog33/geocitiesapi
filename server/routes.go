package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mog33/geocitiesapi/auth"
	"gitlab.com/mog33/geocitiesapi/handler"
)

func Routing(router *gin.Engine, apiVersion string) {
	publicRoutes := router.Group("/")
	{
		// Kubernetes liveness and health probes.
		publicRoutes.GET("/started", handler.Started)
		publicRoutes.GET("/healthz", handler.Health)
	}

	authRoutes := router.Group("/api/" + apiVersion)
	authRoutes.Use(auth.AuthAPIKey())
	{
		authRoutes.GET("/cities", handler.GetCities)
		authRoutes.GET("/city/:geonameid", handler.GetCity)
	}
}
