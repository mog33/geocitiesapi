package main

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/mog33/geocitiesapi/server"
	"gitlab.com/mog33/geocitiesapi/storage"
)

const (
	apiVersion = "v1"
)

// @title        Geo Cities API
// @version      1.0
// @description  Simple city search service based on Geonames data.

// @license.name  GPLv3
// @license.url   https://www.gnu.org/licenses/gpl-3.0.en.html

// @host      localhost:8181
// @BasePath  /

// @securityDefinitions.apikey  ApiKeyAuth
// @in                          header
// @name                        X-API-Key

// @securityDefinitions.apikey  ApiKeyAuth
// @in                          query
// @name                        key

func main() {
	err := godotenv.Load()
	if err == nil {
		log.Print("[notice] Using local .env file")
	} else {
		log.Print("[notice] No .env file found, using ENV values")
	}

	storage.CheckBackend()
	server.StartServer(apiVersion)
}
