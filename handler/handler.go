package handler

import (
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/mog33/geocitiesapi/storage"
)

// Started godoc
// @Summary      Service started
// @Description  Check if service is started and running
// @Tags         status
// @Produce      text/plain; charset=utf-8
// @Success      200  {object}  models.CityList
// @Failure      404  {object}  object
// @Router       /started [get]
func Started(c *gin.Context) {
	c.String(http.StatusOK, "Started")
}

// Health godoc
// @Summary      Service health
// @Description  Check service and backend health
// @Tags         status
// @Produce      text/plain; charset=utf-8
// @Success      200  {object}  models.CityList
// @Failure      404  {object}  object
// @Router       /healthz [get]
func Health(c *gin.Context) {
	err := storage.GetBackend().Health()
	if err != nil {
		c.String(http.StatusInternalServerError, "Error: %s", err.Error())
		return
	}
	c.String(http.StatusOK, "Ok")
}

// GetCities godoc
// @Summary      Search cities
// @Description  Search cities by name and country ISO code
// @Tags         cities
// @Accept       text/plain; charset=utf-8
// @Produce      json
// @Param        q        query     string  true  "Search by city name"
// @Param        country  query     string  true  "Country ISO code"
// @Success      200      {object}  models.CityList
// @Failure      400      {object}  object
// @Failure      404      {object}  object
// @Router       /api/v1/cities [get]
func GetCities(c *gin.Context) {
	search := c.Query("q")
	country := strings.ToUpper(c.Query("country"))

	if search == "" || country == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":   "error",
			"message":  "Missing parameter 'q' and/or 'country' for search.",
			"response": "",
		})
		return
	}

	var limit int
	query_limit := os.Getenv("API_QUERY_LIMIT")
	if query_limit == "" {
		limit = 15
	} else {
		limit, _ = strconv.Atoi(query_limit)
	}

	result, message, err := storage.GetBackend().SearchCities(search, country, limit)

	if err != nil {
		// http.StatusNoContent seems to have a redirect effect.
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"status": "error", "message": err.Error(), "response": ""})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "success", "message": message, "response": result})
}

// GetCity godoc
// @Summary      Get City
// @Description  Get City by name and country ISO code
// @Tags         cities
// @Accept       text/plain; charset=utf-8
// @Produce      json
// @Param        id   path      string  true  "City geoname id"
// @Success      200  {object}  models.CityList
// @Failure      400  {object}  object
// @Failure      404  {object}  object
// @Router       /api/v1/city/{id} [get]
func GetCity(c *gin.Context) {
	geonameid := c.Param("geonameid")
	if geonameid == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":   "error",
			"message":  "Missing parameter 'geonameid'.",
			"response": "",
		})
		return
	}
	result, err := storage.GetBackend().GetCity(geonameid)
	if err != nil {
		// http.StatusNoContent seems to have a redirect effect.
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"status": "error", "message": err.Error(), "response": ""})
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": "success", "message": "Success", "response": result})
}
